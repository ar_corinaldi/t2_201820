package doubly.linked.list.test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.DoublyLinkedList;
import model.data_structures.Node;

@SuppressWarnings("unused")
public class DoublyLinkedListTest {

	private DoublyLinkedList<Double> prueba;

	public void setupEscenario1(){
		prueba = new DoublyLinkedList<Double>();
		int[] numeros = new int[10];

		for (double i = 0; i<10; i++ ) {
			prueba.addFirst(i);
		}
	}
	
	public void testGetHead(){
		setupEscenario1();
		assertEquals("No se asigno correctamente la cabeza del nodo",0,00, prueba.getHead().getElement());
	}
	
	public void testGetTail(){
		setupEscenario1();
		assertEquals("No se asigno correctamente la cabeza del nodo",9,00, prueba.getTail().getElement());
	}
	
	public void testAddNode(){
		setupEscenario1();
		prueba.addNode((double) 99);
		assertEquals("No se agrego el nodo correctamente",99,00, prueba.getHead().getElement());
	}
	
	public void testGetNext(){
		setupEscenario1();
		Node<Double> siguiente = prueba.getHead();
		assertEquals("No al siguiente nodo",1,00, siguiente.getElement());
	}
	
	public void testGetElementByPos(){
		setupEscenario1();
		assertEquals("No se dio el elemento correcto por la posicion del nodo",3,00, prueba.getElementByPos(4));
	}
	
	public void testGetSize(){
		setupEscenario1();
		assertTrue("Los tamanos no concuerdan", 10 == prueba.getSize());
	}
	
}



