package model.vo;

/**
 * Representation of a Station object
 */
public class VOStation {

	private int id;
	
	private String name;
	
	private String city;
	
	private double latitude;
	
	private double longitude;
	
	private int dpcapacity;
	
	private String online_date;
	
	public VOStation( int id, String name, String city, double latitude, double longitude, int dpcapacity, String online_date ){
		this.id = id;
		this.name = name;
		this.city = city;
		this.latitude = latitude;
		this.longitude = longitude;
		this.dpcapacity = dpcapacity;
		this.online_date = online_date;
	}
	
	/**
	 * @return id_bike - Bike_id
	 */
	public int getId() {
		// TODO Auto-generated method stub
		return id;
	}	
	
	public String getName(){return name;}
	
	public String getCity(){return city;}
	
	public double getLatitude(){return latitude;}
	
	public double getLongitude(){return longitude;}
	
	public int getDpcapacity(){return dpcapacity;}
	
	public String getOnline_date(){return online_date;}
}
