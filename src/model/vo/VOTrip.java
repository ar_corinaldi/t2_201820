package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	private int tripID;

	private String start_time;

	private String end_time;

	private int bikeID;

	private double tripDuration;

	private int fromStationID;

	private String fromStation;

	private int toStationID;

	private String toStation;

	private String usertype;

	private String gender;

	private int birthdayYear;


	public VOTrip( int tripID, String start_time, String end_time, int bikeID,int tripDuration, int fromStationID, String fromStation, int toStationID, String toStation, String usertype, String gender, int birthdayYear ){
		this.tripID = tripID;
		this.start_time = start_time;
		this.end_time = end_time;
		this.bikeID = bikeID;
		this.tripDuration = tripDuration;
		this.fromStationID = fromStationID;
		this.fromStation = fromStation;
		this.toStationID = toStationID;
		this.toStation = toStation;
		this.usertype = usertype;
		if( gender == null )
		{
			gender = "No Aplica";
		}
		else{
			this.gender = gender;
		}
		this.birthdayYear = birthdayYear;
	}

	public VOTrip( int tripID, String start_time, String end_time, int bikeID,int tripDuration, int fromStationID, String fromStation, int toStationID, String toStation, String usertype){
		this.tripID = tripID;
		this.start_time = start_time;
		this.end_time = end_time;
		this.bikeID = bikeID;
		this.tripDuration = tripDuration;
		this.fromStationID = fromStationID;
		this.fromStation = fromStation;
		this.toStationID = toStationID;
		this.toStation = toStation;
		this.usertype = usertype;
		gender = "No aplica";
	}

	public VOTrip( int tripID, String start_time, String end_time, int bikeID,int tripDuration, int fromStationID, String fromStation, int toStationID, String toStation, String usertype, String gender ){
		this.tripID = tripID;
		this.start_time = start_time;
		this.end_time = end_time;
		this.bikeID = bikeID;
		this.tripDuration = tripDuration;
		this.fromStationID = fromStationID;
		this.fromStation = fromStation;
		this.toStationID = toStationID;
		this.toStation = toStation;
		this.usertype = usertype;
		if( gender == null )
		{
			gender = "No Aplica";
		}
		else{
			this.gender = gender;
		}
	}


	/**
	 * @return id - Trip_id
	 */
	public int getTripID() {
		// TODO Auto-generated method stub
		return tripID;
	}	


	/**
	 * @return tripDuration - tripDuration of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return tripDuration;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return fromStation;
	}

	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return toStation;
	}


	/**
	 * @return the start_time
	 */
	public String getStart_time() {
		return start_time;
	}


	/**
	 * @return the end_time
	 */
	public String getEnd_time() {
		return end_time;
	}


	/**
	 * @return the fromStationID
	 */
	public int getFromStationID() {
		return fromStationID;
	}

	/**
	 * @return the toStationID
	 */
	public int getToStationID() {
		return toStationID;
	}

	/**
	 * @return the usertype
	 */
	public String getUsertype() {
		return usertype;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}


	public int getBikeID() {
		return bikeID;
	}


	public int getBirthdayYear() {
		return birthdayYear;
	}


	public void setBirthdayYear(int birthdayYear) {
		this.birthdayYear = birthdayYear;
	}

}
