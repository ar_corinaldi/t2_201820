package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Node;
import model.data_structures.DoublyLinkedList;

public class DivvyTripsManager implements IDivvyTripsManager {

	private DoublyLinkedList<VOTrip> tripList;
	
	private DoublyLinkedList<VOStation> stationList;
	
	public DivvyTripsManager(){
		tripList = new DoublyLinkedList<>();
		stationList = new DoublyLinkedList<>();
	}
	
	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub
		try {
			int contador = 1;
			CSVReader reader = new CSVReader(new FileReader(stationsFile));
			String[] linea;
			reader.readNext();
			while( (linea = reader.readNext()) != null){
				int id = Integer.parseInt(linea[0]);
				String name = linea[1];
				String city = linea[2];
				double latitude = Double.parseDouble(linea[3]);
				double longitud = Double.parseDouble(linea[4]);
				int dpcapacity = Integer.parseInt(linea[5]);
				String online_date = linea[6];
				VOStation station = new VOStation(id, name, city, latitude, longitud, dpcapacity, online_date);
				stationList.addFirst(station);
				contador++;
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub
		try {
			CSVReader reader = new CSVReader(new FileReader(tripsFile), '\t');
			VOTrip trip = null;
			String[] linea = reader.readNext();
			while( (linea = reader.readNext()) != null ){
				for (String string: linea) {
					String[] linea2 = string.split(",");
					int tripID = Integer.parseInt(linea2[0]);
					String start_time = linea2[1];
					String end_time = linea2[2];
					int bikeID = Integer.parseInt(linea2[3]);
					int tripDuration = Integer.parseInt(linea2[4]);
					int fromStationID = Integer.parseInt(linea2[5]);
					String fromStation = linea2[6];
					int toStationID = Integer.parseInt(linea2[7]);
					String toStation = linea2[8];
					String usertype = linea2[9];
					String gender = "";
					int birthdayYear = 0;
					if(linea2.length == 10){
						trip = new VOTrip(tripID, start_time, end_time, bikeID, tripDuration, fromStationID, fromStation, toStationID, toStation, usertype);
					}
					else{
						gender = linea2[10];
						if( linea2.length == 12 ){
							birthdayYear = Integer.parseInt(linea2[11] );
							trip = new VOTrip(tripID, start_time, end_time, bikeID, tripDuration, fromStationID, fromStation, toStationID, toStation, usertype, gender, birthdayYear);
						}else{
							trip = new VOTrip(tripID, start_time, end_time, bikeID, tripDuration, fromStationID, fromStation, toStationID, toStation, usertype, gender);
						}
					}
					tripList.addFirst(trip);
				}
				
			} 
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public IDoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		// TODO Auto-generated method stub
		DoublyLinkedList<VOTrip> genderList = new DoublyLinkedList<VOTrip>();
		Node<VOTrip> actual =tripList.getHead();
		while( (actual = actual.getNext()) != null ) {
			if( actual.getElement().getGender().equalsIgnoreCase(gender) ){
				genderList.addFirst(actual.getElement());
			}
		}
		return genderList;
	}

	@Override
	public IDoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		// TODO Auto-generated method stub
		DoublyLinkedList<VOTrip> toStationList = new DoublyLinkedList<VOTrip>();
		Node<VOTrip> actual =tripList.getHead();
		while( (actual = actual.getNext()) != null ) {
			if( actual.getElement().getToStationID() == stationID ){
				toStationList.addFirst(actual.getElement());
			}
		}
		
		return toStationList;
	}	


}
