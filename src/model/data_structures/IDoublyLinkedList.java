package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<T> extends Iterable<T> {

	Integer getSize();
	
	void addFirst( T e );
	
	void addLast( T e );
	
	void removeNode( T e );
	
	T getElementByPos( int pPos );
	
	void setFirstNode();
	
	Node<T> getHead();
	
	Node<T> getTail();
	
	Node<T> getNext();
	
	Node<T> getPrevious();
}

