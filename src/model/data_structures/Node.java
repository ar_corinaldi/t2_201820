package model.data_structures;

public class Node<T> {
	
	private T element;
	
	private Node<T> next;
	
	private Node<T> previous;
	
	public Node( T e, Node<T> pNext ){
		element = e;
		next = pNext;
		previous = null;
	}
	
	public Node<T> getNext(){
		return next;
	}
	
	public void setNext( Node<T> n ){
		next = n;
	}
	
	public Node<T> getPrevious(){
		return previous;
	}
	
	public void setPrevious( Node<T> n ){
		previous = n;
	}
	
	public T getElement(){
		return element;
	}
	
	public void setElement( T e ){
		element = e;
	}
}
