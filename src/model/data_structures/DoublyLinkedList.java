package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList<T> implements IDoublyLinkedList<T> {

	private Node<T> head;

	private Node<T> tail;

	private Node<T> actual;

	private int size;

	public DoublyLinkedList(){
		actual = null;
		tail=null;
		head=null;
		size=0;
	}

	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub
		return size;
	}

	public boolean isEmpty(){
		return size == 0;
	}

	@Override
	public void addFirst( T e ) {
		// TODO Auto-generated method stub
		if( head == null )
		{
			head = new Node<T>( e, head );
			tail = head;
		}
		else
		{
			Node<T> nuevo = new Node<T>( e, head );

			head.setPrevious(nuevo);
			head = nuevo;
		}
		size++;
	}

	public void addLast( T e ){
		Node<T> newest = new Node<>(e, null);
		if( isEmpty() ){
			head = newest;
		}
		else{
			tail.setNext(newest);
		}
		tail = newest;
		size++;
	}
	
	public Node<T> buscarNode( T e ){
		actual = head;
		while( actual != null ){
			if( actual.getElement().equals(e) ){
				return actual;
			}
			actual = actual.getNext();
		}
		return null;
	}
	
	@Override
	public void removeNode( T e ) {
		// TODO Auto-generated method stub

		if( isEmpty() ){
			System.out.println("No hay lista");
		}
		else if( buscarNode(e) == null ){
			System.out.println("No se encontro el nodo con el elemento");
		}
		else{
			if( head.getElement().equals(e) ){
				head.getNext().setPrevious(null);
				head = head.getNext();
			}
			else if( buscarNode(e) != null ){
				buscarNode(e).getPrevious().setNext(buscarNode(e).getNext());
				buscarNode(e).getNext().setPrevious(buscarNode(e).getPrevious());
			}
			size--;
		}

	}

	@Override
	public T getElementByPos(int pPos) {
		// TODO Auto-generated method stub
		T e = null;
		actual = head;
		if( isEmpty() ){
			System.out.println("No hay lista");
			return e;
		}

		int contador=1;
		while( actual != null ){
			if( contador == pPos ){
				e = actual.getElement();
				break;
			}
			actual = actual.getNext();
			contador++;
		}

		return e;
	}

	@Override
	public void setFirstNode() {
		// TODO Auto-generated method stub
		actual = head;
	}
	
	public Node<T> getHead(){
		return head;
	}
	
	public Node<T> getTail(){
		return tail;
	}

	@Override
	public Node<T> getNext() {
		// TODO Auto-generated method stub
		return actual.getNext();
	}

	@Override
	public Node<T> getPrevious() {
		// TODO Auto-generated method stub
		return actual.getPrevious();
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
